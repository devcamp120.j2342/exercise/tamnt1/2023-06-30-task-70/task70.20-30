package com.devcamp.country.response;

import java.util.List;

import com.devcamp.country.models.Region;

public class CustomCountryResponse {
    private Long id;
    private String countryCode;
    private String countryName;
    private Long regionTotal;
    private List<Region> regions;

    public CustomCountryResponse() {
    }

    public CustomCountryResponse(String countryCode, String countryName, Long regionTotal, List<Region> regions) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.regionTotal = regionTotal;
        this.regions = regions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Long getRegionTotal() {
        return regionTotal;
    }

    public void setRegionTotal(Long regionTotal) {
        this.regionTotal = regionTotal;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

}