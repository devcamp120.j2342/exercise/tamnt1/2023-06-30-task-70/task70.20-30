package com.devcamp.country.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.country.models.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {
    List<Region> findByCountryId(Long countryId);

    long countByCountryId(Long countryId);

    boolean existsById(Long regionId);

}
