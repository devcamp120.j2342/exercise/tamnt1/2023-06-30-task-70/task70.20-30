package com.devcamp.country.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.country.models.Country;
import com.devcamp.country.response.CustomCountryResponse;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findByCountryCode(String countryCode);

    List<Country> findByCountryName(String countryName);

    List<Country> findByRegionsRegionCode(String regionCode);

    List<Country> findByRegionsRegionName(String regionName);

    @Query("SELECT new com.devcamp.country.response.CustomCountryResponse(c.id, c.countryCode, c.countryName, COUNT(r.country.id), "
            + "(SELECT JSON_ARRAYAGG(JSON_OBJECT('id', reg.id, 'regionCode', reg.regionCode, 'regionName', reg.regionName)) "
            + "FROM Region reg WHERE reg.country = c)) "
            + "FROM Country c "
            + "LEFT JOIN c.regions r "
            + "GROUP BY c.id, c.countryCode, c.countryName")
    List<CustomCountryResponse> getCustomCountryResponses();
}
