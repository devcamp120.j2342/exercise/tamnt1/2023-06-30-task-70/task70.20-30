package com.devcamp.country.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.country.models.Country;
import com.devcamp.country.response.CustomCountryResponse;
import com.devcamp.country.services.CountryService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CountryController {
    private final CountryService countryService;

    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/country-list")
    public ResponseEntity<List<Country>> getCountryList() {
        try {
            List<Country> countryList = countryService.getAllCountries();
            return new ResponseEntity<>(countryList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country/{id}")
    public ResponseEntity<Country> getCountryById(@PathVariable(value = "id") long id) {
        try {
            Country country = countryService.getCountryById(id);
            return new ResponseEntity<>(country, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country")
    public ResponseEntity<Country> getCountryByCode(
            @RequestParam(value = "id", defaultValue = "") String countryCode) {
        try {
            Country country = countryService.getCountryByCountryCode(countryCode);
            return new ResponseEntity<>(country, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country-by-name")
    public ResponseEntity<List<Country>> getCountriesByCountryName(
            @RequestParam(value = "countryName") String countryName) {
        try {
            List<Country> countries = countryService.getCountriesByCountryName(countryName);
            return new ResponseEntity<>(countries, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country-by-region-code")
    public ResponseEntity<List<Country>> getCountriesByRegionCode(
            @RequestParam(value = "regionCode") String regionCode) {
        try {
            List<Country> countries = countryService.getCountriesByRegionCode(regionCode);
            return new ResponseEntity<>(countries, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country-region-count")
    public ResponseEntity<List<CustomCountryResponse>> getCountriesWithTotalRegion() {
        try {
            List<CustomCountryResponse> countriesWithRegions = countryService.getCountriesWithRegions();
            return new ResponseEntity<>(countriesWithRegions, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country-by-region-name")
    public ResponseEntity<List<Country>> getCountriesByRegionName(
            @RequestParam(value = "regionName") String regionName) {
        try {
            List<Country> countries = countryService.getCountriesByRegionName(regionName);
            return new ResponseEntity<>(countries, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/country")
    public ResponseEntity<Country> createCountry(@RequestBody Country country) {
        try {
            Country newCountry = countryService.createCountry(country);
            return new ResponseEntity<>(newCountry, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/country/{id}")
    public ResponseEntity<Country> updateCountry(
            @PathVariable(value = "id") long id, @RequestBody Country country) {
        try {
            Country newCountry = countryService.updateCountry(id, country);
            return new ResponseEntity<>(newCountry, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
