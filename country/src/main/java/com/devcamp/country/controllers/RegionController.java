package com.devcamp.country.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.country.models.Region;
import com.devcamp.country.services.RegionService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RegionController {
    @Autowired
    private final RegionService regionService;

    public RegionController(RegionService RegionService) {
        this.regionService = RegionService;
    }

    @GetMapping("/region-list")
    public ResponseEntity<List<Region>> getRegionList() {
        try {
            List<Region> regionList = regionService.getAllRegions();
            return new ResponseEntity<>(regionList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/region/{id}")
    public ResponseEntity<Region> getRegionById(@PathVariable(value = "id") long id) {
        try {
            Region Region = regionService.getRegionById(id);

            return new ResponseEntity<>(Region, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/region")
    public ResponseEntity<List<Region>> getRegionsByCountryId(
            @RequestParam(value = "countryId", defaultValue = "") long countryId) {
        try {

            List<Region> regionList = regionService.getRegionsByCountryId(countryId);

            return new ResponseEntity<>(regionList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/region")
    public ResponseEntity<Region> createRegion(@RequestBody Region region) {
        try {
            Region newRegion = regionService.createRegion(region);
            return new ResponseEntity<Region>(newRegion, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/region/{id}")
    public ResponseEntity<Region> updateRegion(@PathVariable(value = "id") long id, @RequestBody Region region) {
        try {
            Region newRegion = regionService.updateRegion(id, region);
            return new ResponseEntity<Region>(newRegion, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Long> countRegionsByCountryId(@RequestParam(value = "countryId") long countryId) {
        try {
            long count = regionService.countRegionsByCountryId(countryId);
            return new ResponseEntity<>(count, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/region/check")
    public ResponseEntity<Boolean> checkRegionsByCountryId(@RequestParam(value = "countryId") long countryId) {
        try {
            boolean exists = regionService.checkRegionById(countryId);
            return new ResponseEntity<>(exists, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
