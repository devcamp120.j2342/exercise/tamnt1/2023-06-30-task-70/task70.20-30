package com.devcamp.country.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.devcamp.country.models.Region;
import com.devcamp.country.repository.RegionRepository;

@Service
public class RegionService {
    private final RegionRepository regionRepository;

    public RegionService(RegionRepository regionRepository) {
        this.regionRepository = regionRepository;
    }

    public List<Region> getAllRegions() {
        return regionRepository.findAll();
    }

    public List<Region> getRegionsByCountryId(long id) {
        return regionRepository.findByCountryId(id);
    }

    public Region getRegionById(long id) {
        Optional<Region> optionalRegion = regionRepository.findById(id);
        if (optionalRegion.isPresent()) {
            return optionalRegion.get();
        } else {
            return null;
        }
    }

    public Region createRegion(Region region) {
        return regionRepository.save(region);
    }

    public Region updateRegion(long id, Region region) {
        Optional<Region> optionalRegion = regionRepository.findById(id);
        if (optionalRegion.isPresent()) {
            Region existingRegion = optionalRegion.get();
            existingRegion.setRegionCode(region.getRegionCode());
            existingRegion.setRegionName(region.getRegionName());
            existingRegion.setCountry(region.getCountry());
            return regionRepository.save(existingRegion);

        } else {
            return null;
        }

    }

    public void deleteRegion(long id) {
        regionRepository.deleteById(id);
    }

    public boolean checkRegionById(long id) {
        return regionRepository.existsById(id);
    }

    public long countRegionsByCountryId(long countryId) {
        return regionRepository.countByCountryId(countryId);
    }
}
