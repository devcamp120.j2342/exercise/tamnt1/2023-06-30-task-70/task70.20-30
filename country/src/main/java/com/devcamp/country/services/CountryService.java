package com.devcamp.country.services;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.devcamp.country.models.Country;
import com.devcamp.country.repository.CountryRepository;
import com.devcamp.country.response.CustomCountryResponse;

@Service
public class CountryService {
    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    public Country getCountryById(long id) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if (optionalCountry.isPresent()) {
            return optionalCountry.get();
        } else {
            return null;
        }
    }

    public List<Country> getCountriesByCountryName(String countryName) {
        return countryRepository.findByCountryName(countryName);
    }

    public List<Country> getCountriesByRegionCode(String regionCode) {
        return countryRepository.findByRegionsRegionCode(regionCode);
    }

    public List<Country> getCountriesByRegionName(String regionName) {
        return countryRepository.findByRegionsRegionName(regionName);
    }

    public Country getCountryByCountryCode(String countryCode) {
        return countryRepository.findByCountryCode(countryCode);

    }

    public Country createCountry(Country country) {
        return countryRepository.save(country);
    }

    public Country updateCountry(long id, Country country) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if (optionalCountry.isPresent()) {
            Country existingCountry = optionalCountry.get();
            existingCountry.setCountryCode(country.getCountryCode());
            existingCountry.setCountryName(country.getCountryName());
            existingCountry.setRegions(country.getRegions());
            return countryRepository.save(existingCountry);

        } else {
            return null;
        }

    }

    public void deleteCountry(long id) {
        countryRepository.deleteById(id);
    }

    public List<CustomCountryResponse> getCountriesWithRegions() {
        return countryRepository.getCustomCountryResponses();
    }
}
